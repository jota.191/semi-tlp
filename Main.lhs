\documentclass{beamer}
\usepackage[utf8]{ inputenc }
\usetheme{boxes}
%\usecolortheme{beaver}

\usepackage{xcolor}

\makeatletter
\newenvironment{noheadline}{
    \setbeamertemplate{headline}{}
    \addtobeamertemplate{frametitle}{\vspace*{-0.9\baselineskip}}{}
}{}
\makeatother



\title{Programación a nivel de tipos en Haskell}
\subtitle{Como hacer más expresivo al sistema de tipos}
\author{Juan García Garland}
\institute{\{ IMERL - InCo \} - FIng - UdelaR}
\date{Marzo 2019}


%include polycode.fmt

%format ::: = "\: `\! \! :"
%format € = "\: `("
%format ^ = "\: `["
%format forall = "\forall"
%format . = "."
%format .+. = "\oplus"
%format .++. = "\bigoplus"

\begin{document}

\frame{
  \titlepage
}

%if False

> {-# LANGUAGE GADTs #-}
> {-# LANGUAGE MultiParamTypeClasses #-}
> {-# LANGUAGE FlexibleInstances      #-}
> {-# LANGUAGE FlexibleContexts       #-}
> {-# LANGUAGE UndecidableInstances   #-}
> {-# LANGUAGE FunctionalDependencies #-}
> {-# LANGUAGE ScopedTypeVariables    #-}
> {-# LANGUAGE DataKinds              #-}
> {-# LANGUAGE TypeFamilies    #-}
> {-# LANGUAGE PolyKinds       #-}
> {-# LANGUAGE KindSignatures  #-}
> {-# LANGUAGE TypeOperators   #-}
> {-# LANGUAGE TypeApplications #-}
> {-# LANGUAGE ConstraintKinds #-}

> import Data.Kind

%endif

\frame{\frametitle{Tipos algebraicos en Haskell}

\begin{columns}
\begin{column}{0.5\textwidth}

< data Bool
<   =  True
<   |  False

\pause

> data Nat
>   =  Zero
>   |  Succ Nat

%if False

>   deriving (Show)

%endif

\end{column}\pause
\begin{column}{0.5\textwidth}


> data List a
>   = Nil
>   | Cons a (List a)

\pause

> data Exp
>   =  ValN  Nat
>   |  ValB  Bool
>   |  Add   Exp Exp
>   |  If    Exp Exp Exp


\end{column}
\end{columns}

}

\frame{\frametitle{Tipos algebraicos en Haskell}

< data Nat
<   =  Zero
<   |  Succ Nat

< Zero :: Nat
< Succ :: Nat -> Nat

< Nat :: Type  -- aka *

\pause

< List :: Type -> Type

}


\frame{\frametitle{Tipos algebraicos \emph{generalizados} en Haskell}

< {-# LANGUAGE GADTs #-}
  
< data Nat where
<   Zero :: Nat
<   Succ :: Nat -> Nat

< Zero :: Nat
< Succ :: Nat -> Nat

< Nat :: Type  -- aka *

}

\frame{

< data Exp
<   =  ValN  Nat
<   |  ValB  Bool
<   |  Add   Exp Exp
<   |  If    Exp Exp Exp

\pause

< data Exp where
<   ValN   :: Nat -> Exp
<   ValB   :: Bool -> Exp
<   Add    :: Exp  -> Exp -> Exp
<   If     :: Exp  -> Exp -> Exp

}

\frame{

< data Exp where
<   ValN   :: Nat -> Exp
<   ValB   :: Bool -> Exp
<   Add    :: Exp  -> Exp -> Exp
<   If     :: Exp  -> Exp -> Exp

\pause
\begin{alertblock}{No representable sin GADTs}

< data Exp a where
<   ValN   :: Nat -> Exp Nat
<   ValB   :: Bool -> Exp Bool
<   Add    :: Exp Nat  -> Exp Nat -> Exp Nat
<   If     :: Exp Bool -> Exp a   -> Exp a

\end{alertblock}
}


\frame{\frametitle{Truco: Naturales a nivel  de tipos}

< data Zero
< data Succ n

\pause

< Zero :: Type
< Succ :: Type -> Type

\pause

Problema: !`$Succ \: Bool$ es un tipo válido! 

}

\frame{\frametitle{Truco: Vectores indizados por su largo}

> data Vec n a where
>   VZ :: Vec Zero a
>   VS :: a -> Vec n a -> Vec (Succ n) a

\pause

> vHead :: Vec (Succ n) a -> a
> vHead (VS a _)  = a 


> vZipWith :: (a -> b -> c) -> Vec n a -> Vec n b -> Vec n c
> vZipWith _  VZ         VZ         = VZ
> vZipWith f  (VS x xs)  (VS y ys)  = VS (f x y)(vZipWith f xs ys)

}
\frame{

< vHead :: Vec (Succ n) a -> a
< vHead (VS a _)  = a 

\pause

< \ >vHead VZ
<     - Couldn't match type ‘Zero’ with ‘Succ n0’
<       Expected type: Vec (Succ n0) a
<         Actual type: Vec Zero a

\pause

Problema: !`$Vec \: Bool \: Char $ es un tipo válido!

}



\frame{\frametitle{Polimorfismo ad-hoc}

< class Eq a where
<   (==) :: a -> a -> Bool

> instance Eq Nat where
>   Zero    ==  Zero    = True
>   Succ m  ==  Succ n  = m == n
>   _       == _        = False

< instance Eq a => Eq [a] where
<   []        ==  []        = True
<   (x : xs)  ==  (y : ys)  = x == y && xs == ys
<   _         ==   _        = False

}

\frame{\frametitle{Polimorfismo ad-hoc}

Hindley-Milner es decidible. Hindley-Milner + Typeclasses?
\pause

Existen restricciones a la hora de definir instancias de
typeclasses, o contextos de funciones para asegurar decibilidad.

Son ilegales:

< instance Eq a => Eq (a,Char)
< instance Eq [[a]]
< instance Eq String
< f :: (Eq [a]) => [a] -> [a] -> Bool

}

\frame{\frametitle{Polimorfismo ad-hoc: extensiones}

< {-# LANGUAGE FlexibleInstances      #-}
< {-# LANGUAGE FlexibleContexts       #-}
< {-# LANGUAGE UndecidableInstances   #-}

\pause
y más...

}

\frame{\frametitle{Más sobre typeclasses}

Una clase puede interpretarse como un conjunto de tipos,
o un predicado sobre tipos.

\pause

?`Por qué el lenguaje no soporta relaciones, o predicados $n$-arios?

> class Iso a b where
>   iso :: a -> b
>   osi :: b -> a

\pause Simplemente por una decisión de diseño.

< {-# LANGUAGE MultiParamTypeClasses #-}

}


\frame{\frametitle{Ejemplo}

< class Eq e => Collection c e where
<   insert :: c -> e -> c
<   member :: c -> e -> Bool
<   ...

< instance Eq a => Collection [a] a where
<   insert = flip (:)
<   member = flip elem
<   ...

}

\frame{\frametitle{Ejemplo}

< class Eq e => Collection c e where
<   insert :: c -> e -> c
<   member :: c -> e -> Bool
<   empty  :: c -> Bool
<   ...

\pause


<  error:
<     Couldn't deduce (Collection c e0)
<       from the context: Collection c e
<         bound by the type signature for:
<                    empty :: forall c e,  Collection c e => c -> Bool
<       The type variable 'e0' is ambiguous

}



\frame{\frametitle{Dependencias funcionales}

> {-# LANGUAGE FunctionalDependencies #-}

< class Eq e => Collection c e | c -> e where
<   insert :: c -> e -> c
<   member :: c -> e -> Bool
<   empty  :: c -> Bool
<   ...

< instance Eq a => Collection [a] a where
<   insert = flip (:)
<   member = flip elem
<   empty  = null 
<   ... 

}


\frame{\frametitle{Truco: Programando \emph{a la} prolog a nivel de tipos}

{\tt MultiParamTypeClasses} + {\tt FunctionalDependencies} \pause
= !`Funciones!

\pause


< class Add m n k | m n -> k where
<   tAdd :: m -> n -> k

\pause

< instance Add Zero m m where
<   tAdd = undefined

< instance Add m n k
<   => Add (Succ m) n (Succ k) where
<   tAdd = undefined



}

\frame{\frametitle{Truco: Programando \emph{a la} prolog a nivel de tipos}

< class Add m n k | m n -> k where
<   tAdd :: m -> n -> k
< instance Add Zero m m where
<   tAdd = undefined
< instance Add m n k
<   => Add (Succ m) n (Succ k) where
<   tAdd = undefined


< \ > tAdd (undefined :: Succ (Succ Zero))(undefined :: Succ Zero)

}

\frame{\frametitle{Truco: Programando \emph{a la} prolog a nivel de tipos}

< class Add m n k | m n -> k where
<   tAdd :: m -> n -> k
< instance Add Zero m m where
<   tAdd = undefined
< instance Add m n k
<   => Add (Succ m) n (Succ k) where
<   tAdd = undefined


< \ > tAdd (undefined :: Succ (Succ Zero))(undefined :: Succ Zero)
< tAdd (undefined :: Succ (Succ Zero))(undefined :: Succ Zero)
<  :: Succ (Succ (Succ Zero))

\pause

Problema: !`$Add \: (Succ Zero) \: Bool \: (Succ Zero) $
es una \emph{constraint} que se satisface! 


}


\frame{\frametitle{Simulando tipos dependientes }

< vAppend :: (Add m n k) => Vec m a -> Vec n a -> Vec k a
< vAppend VZ         bs  = bs
< vAppend (VS a as)  bs  = VS a (vAppend as bs)

\begin{center}{\color{red} !`NO!}\end{center}


}\frame{\frametitle{Simulando tipos dependientes}

< class (Add m n k) => VAppend m n k where
<   vAppend :: Vec m a -> Vec n a -> Vec k a
< instance VAppend Zero m m where
<   vAppend VZ ys = ys 
< instance VAppend m n k
<   => VAppend (Succ m) n (Succ k) where
<   vAppend (VS x xs) (ys :: Vec n a)
<     = VS x (vAppend xs ys :: Vec k a)


}


\frame{\frametitle{Clausurando nuestras construcciones}

> class TNat n
> instance TNat Zero
> instance TNat n => TNat (Succ n)

\pause
< data Vec n a where
<   VZ :: Vec Zero a
<   VS :: (TNat n) => a -> Vec n a -> Vec (Succ n) a

\pause

< class (TNat m, TNat n, TNat k) => Add m n k | m n -> k where
<   tAdd :: m -> n -> k

\pause etc... Esto también es "prolog-like".

}


\frame{
\huge

Listas Heterogeneas


}

\frame{\frametitle{Listas heterogeneas ({\it a la} HList)}

> data HNil = HNil           
> data HCons e l = HCons e l 


\pause
Funciona para codificar listas heterogeneas

> buena :: HCons Bool (HCons String HNil)
> buena = HCons True (HCons "" HNil)

\pause

De nuevo, problema...

> mala :: HCons Bool (HCons Integer Char)
> mala = HCons True (HCons 14 'k')

}


\frame{\frametitle{Listas heterogeneas ({\it a la} HList)}

Solución, de nuevo, clausura:

< data HNil = HNil
< data HCons e l = HCons e l

> class HList l
> instance HList HNil
> instance HList l => HList (HCons e l)

> hNil  :: HNil
> hNil  =  HNil

> hCons :: HList l => e -> l -> HCons e l
> hCons e l = HCons e l

}



\frame{\huge Registros Heterogeneos}

\frame{\frametitle{Registros heterogeneos}

Son colecciones indizadas por tipos.

\pause

Idea:
Construir registros heterogeneos a partir de listas
heterogeneas con campos del tipo:

> data Tagged l v = Tagged v

\pause
Para asegurar buena formaci\'on es  necesario chequear:
\hfill\break
\begin{itemize}
  \item Todos los campos son $Tagged$
  \item No se repiten etiquetas
\end{itemize}
}

\frame{\frametitle{Registros heterogeneos correctos por construcci\'on}
\small

< newtype Record r = Record r

< mkRecord :: HRLabelSet r => HList r -> Record r
< mkRecord = Record

\pause



< class ( RecordLabels r ls
<       , HLabelSet ls
<       , HAllTaggedLV r)
<   => HRLabelSet r
< instance ( RecordLabels r ls
<          , HLabelSet ls
<          , HAllTaggedLV r)
<   => HRLabelSet r

}


\frame{\frametitle{Registros heterogeneos correctos por construcci\'on}
\small

< class RecordLabels r ls | r -> ls
< instance RecordLabels HNil HNil
< instance RecordLabels r' ls
<       => RecordLabels (HCons (LVPair l v) r') (HCons l ls)


< class HLabelSet ls
< instance HLabelSet HNil
< instance HLabelSet (HCons x HNil)
< instance ( HEqK l1 l2 leq
<          , HLabelSet' l1 l2 leq r
<          ) => HLabelSet (HCons l1 (HCons l2 r))

< class HLabelSet' l1 l2 leq r
< instance ( HLabelSet (HCons l2 r)
<          , HLabelSet (HCons l1 r)
<          ) => HLabelSet' l1 l2 HFalse r

}

\frame{\frametitle{Registros heterogeneos correctos por construcci\'on}

> class HAllTaggedLV ps
> instance HAllTaggedLV HNil
> instance (HAllTaggedLV xs)
>   => HAllTaggedLV (HCons (Tagged t v) xs)

}





\frame{\frametitle{Conclusiones}
Ventajas: Funciona, logramos expresar información en el tipado
que no es representable en Haskell{98,2010}

\pause
Desventajas: Costoso, verborr\'agico, poco ``idiom\'atico''
(programamos a la prolog)

\pause
A nivel de tipos estamos validando, no tipando

< HCons  :: Type -> Type -> Type
< Record :: Type -> Type

}


\frame{
\huge

Extensiones modernas

\pause

\small

Nos permiten hacer tipada la programación a nivel de tipos, y más. 

}


\frame{\frametitle{datakinds}


< data Nat = Zero | Succ Nat

\pause

introduce:

< Nat  ::  Type
< Zero ::  Nat
< Succ ::  Nat -> Nat

\pause Con 

< {-# LANGUAGE DataKinds       #-}

además introducimos el kind $Nat$ y constructores de tipo:

< Zero :: Nat
< Succ :: Nat -> Nat

}


\frame{\frametitle{datakinds}

< ^Bool, Char ] :: [Type]
< ^Succ Zero, Zero ] :: [Nat]

\pause

{\color{red}

< ^ Zero, Char ] -- no "tipa"

}
}




\frame{\frametitle{Type Families (sobrecarga de tipos)}

< {-# LANGUAGE TypeFamilies     #-}
< {-# LANGUAGE TypeOperators    #-}
< {-# LANGUAGE KindSignatures   #-}

< type family (m :: Nat) + (n :: Nat) :: Nat
< type instance Zero + n = n
< type instance Succ m  + n = Succ (m + n) 

\pause

o bien:

> type family (m :: Nat) + (n :: Nat) :: Nat where
>   (+) Zero  a     = a
>   (+) (Succ a) b  = Succ (a + b)


}

\frame{\frametitle{polykinds}

< {-# LANGUAGE PolyKinds #-}

< type family Length ( list :: [a] ):: Nat where
<    Length ^]          =  Zero
<    Length (x ::: xs)  =  Succ (Length xs)


}

\frame{\frametitle{Vectores, de nuevo}

< data Vec :: Nat -> Type -> Type where
<   VZ  :: Vec Zero a
<   VS  :: a -> Vec n a -> Vec (Succ n) a

}


\frame{\frametitle{Más azucar sintáctica}

< import GHC.TypeLits

< data Vec :: Nat -> Type -> Type where
<   E    :: Vec 0 a
<   (:<) :: a -> Vec n a -> Vec (n+1) a

}

\frame{\frametitle{Recordemos}

> vTail :: Vec (Succ n) a -> Vec n a
> vTail (VS _ as) = as

< vZipWith :: (a -> b -> c) -> Vec n a -> Vec n b -> Vec n c
< vZipWith _ VZ VZ = VZ
< vZipWith f (VS x xs) (VS y ys)
<   = VS (f x y)(vZipWith f xs ys)

\pause

Magia, esto compila:

> vAppend :: Vec n a -> Vec m a -> Vec (n + m) a
> vAppend (VZ) bs      = bs
> vAppend (VS a as) bs = VS a (vAppend as bs)


}


\frame{\frametitle{Programando con "tipos dependientes"}

< vchop :: Vec (m + n) x -> (Vec m x, Vec n x) 


< vtake :: Vec (m + n) x -> Vec m x

}

\frame{\frametitle{Programando con "tipos dependientes": no es tan fácil}

< vchop :: Vec (m + n) x -> (Vec m x, Vec n x)

m, n no viven en runtime 

< vtake :: Vec (m + n) x -> Vec m x
\pause
lo mismo, y algo más sutil

}

\frame{\frametitle{Singletons}

Mapeando valores y tipos \pause

> data SNat :: Nat -> Type where
>   SZ :: SNat Zero
>   SS :: SNat n -> SNat (Succ n)

\pause

> vChop :: SNat m -> Vec (m + n) x -> (Vec m x, Vec n x)
> vChop SZ xs             = (VZ, xs)
> vChop (SS m) (VS x xs)  = let (ys, zs) = vChop m xs
>                           in (VS x ys, zs)

}

\frame{\frametitle{}

> vTake :: SNat m -> Vec (m + n) x -> Vec m x
> vTake SZ xs             = VZ
> vTake (SS m) (VS x xs)  = vTake m xs


}


\frame{\frametitle{}
\color{red}

> vTake :: SNat m -> Vec (m + n) x -> Vec m x
> vTake SZ xs     = VZ
> vTake (SS m) (VS x xs) = vTake m xs

\pause

< Expected type: SNat m -> Vec (m + n) x -> Vec m x
<         Actual type: SNat m -> Vec (m + n0) x -> Vec m x
<       NB: ‘+’ is a non-injective type family
<       The type variable ‘n0’ is ambiguous

\color{black}

No podemos deducir $n$ de $m + n$ sin la información de que
$(+)$ es inyectiva.

}

\frame{\frametitle{proxies}

Haciendo explícitos los tipos:

\pause

> data Proxy :: k -> * where
>   Proxy :: Proxy a

\pause

> vTake :: SNat m -> Proxy n -> Vec (m + n) x -> Vec m x
> vTake SZ      _  xs         = VZ
> vTake (SS m)  n  (VS x xs)  = VS x (vTake m n xs)

}

\frame{
\huge

Listas heterogeneas (modernas)


}

\frame{\frametitle{Listas heterogeneas}


< data HList (l :: [Type]) :: Type  where
<   HNil  :: HList ^]
<   HCons :: x -> HList xs -> HList (x ::: xs) 

\pause

< type family UpdateAtNat  (n :: Nat)
<                          (x :: Type)(xs :: [Type]) :: [Type]
< type instance UpdateAtNat Zero      x  (y ::: ys)
<    = x ::: ys
< type instance UpdateAtNat (Succ n)  x  (y ::: ys)
<    = y ::: UpdateAtNat n x ys

< updateAtNat :: SNat n -> x -> HList xs
<             -> HList (UpdateAtNat n x xs)
< updateAtNat SZ y (HCons _ xs)
<    = HCons y xs
< updateAtNat (SS n) y (HCons x xs)
<    = HCons x (updateAtNat n y xs)


}



\frame{

Otra forma:


< class UpdateAtNat (n :: Nat) (y :: Type) (xs :: [Type]) where
<   type UpdateAtNatR n y xs :: [Type]
<   updateAtNat :: SNat n -> y -> HList xs
<               -> HList (UpdateAtNatR n y xs)

< instance UpdateAtNat Zero y (x ::: xs) where
<   type UpdateAtNatR Zero y (x ::: xs) = (y ::: xs)
<   updateAtNat SZ y (HCons _ xs) = HCons y xs

< instance UpdateAtNat n y xs
<   => UpdateAtNat (Succ n) y (x € xs) where
<   type UpdateAtNatR (Succ n) y (x ::: xs)
<     =  x ::: UpdateAtNatR n y xs
<   updateAtNat (SS n) y (HCons x xs)
<     = HCons x (updateAtNat n y xs)

}

\frame{\huge Registros extensibles fuertemente tipados}

\frame{\frametitle{Registros extensibles fuertemente tipados}
  
< data Record :: forall k . [(k,Type)] -> Type where
<   EmptyR :: Record ^]
<   ConsR  :: LabelSet ( €l, v) ::: xs) =>
<    Tagged l v -> Record xs -> Record ( €l,v) ::: xs)

\pause

Esto es más de lo que parece...

}

\frame{\frametitle{Registros extensibles fuertemente tipados}
  
> class LabelSet (l :: [(k1,k2)])

< instance LabelSet ^]
< instance LabelSet ^ €x,v)]
< instance ( LabelSet' €l1,v1) €l2,v2) (l1==l2) r)
<         => LabelSet ( €l1,v1) ::: €l2,v2) ::: r)

< class LabelSet' l1v1 l2v2 (leq::Bool) r
< instance ( LabelSet ( €l2,v2) ::: r)
<          , LabelSet ( €l1,v1) ::: r)
<          ) => LabelSet' €l1,v1) €l2,v2) False r

}


\frame{
?`Qué más?

\begin{itemize}
\item Funciones sobre records como $lookUp$ son problemáticas, hay más técnicas

\item ?`Para qué sirve todo esto?

\end{itemize}



}



\frame{FIN}\frame{}



\frame{\frametitle{Programando sobre Registros}
\small

> class HasFieldRec (l::k) (r :: [(k,Type)]) where
>   type LookupByLabelRec l r :: Type
>   lookupByLabelRec :: Label l -> Record r
>                    -> LookupByLabelRec l r

\pause

> instance (HasFieldRec' (l == l2) l ( €l2,v) ::: r)) =>
>   HasFieldRec l ( €l2,v) ::: r) where
>   type LookupByLabelRec l ( €l2,v) ::: r)
>     = LookupByLabelRec' (l == l2) l ( €l2,v) ::: r)
>   lookupByLabelRec :: Label l -> Record ( €l2,v) ::: r)
>                    -> LookupByLabelRec l ( €l2,v) ::: r)
>   lookupByLabelRec l r
>     = lookupByLabelRec' (Proxy :: Proxy (l == l2)) l r 

}

\frame{
\small

> class HasFieldRec' (b::Bool) (l::k) (r :: [(k,Type)]) where
>   type LookupByLabelRec' b l r :: Type
>   lookupByLabelRec' ::
>      Proxy b -> Label l -> Record r
>              -> LookupByLabelRec' b l r

> instance HasFieldRec'    True l ( €l, v) ::: r) where
>   type LookupByLabelRec' True l ( €l, v) ::: r)
>     = v
>   lookupByLabelRec' _ _ (ConsR lv _)
>     = unTagged lv

> instance (HasFieldRec l r )=>
>   HasFieldRec' False l ( €l2, v) ::: r) where
>   type LookupByLabelRec' False l ( €l2, v) ::: r)
>     = LookupByLabelRec l r
>   lookupByLabelRec' _ l (ConsR _ r)
>     = lookupByLabelRec l r

}



\end{document}



Haskell es un lenguaje funcional puro fuerte y estáticamente tipado.
El sistema de tipos de Haskell se basa en el tipado de Hindley-Milner
con algunas extensiones (notablemente el sistema de typeclasses).
Estos sistemas proveen un nivel considerable de protección de tipos
(\emph{type safety}). Sin embargo en Haskell los lenguajes de tipos
y de términos están estrictamente separados, en un principio
no es posible en particular escribir tipos dependientes,
para representar por ejemplo familias inductivas como los vectores
indizados por su longitud.

Es posible codificar a nivel de tipos "copias" de los tipos de datos,
usando tipos para representar valores y typeclasses para representar funciones,
mediante el uso y abuso de extensiones que implementa el compilador GHC
(originalmente diseñadas para otros propósitos).
Estas técnicas, llamadas "programación a nivel de tipos"
(\emph{"Type Level Programming"}) en la literatura, nos permiten codificar
tipos más expresivos (que aseguren más propiedades en tiempo de compilación),
como familias indizadas o registros heterogeneos.
En la última década han surgido nuevas extensiones diseñadas directamente
para soportar este estilo de programación, proveyendo además la posibilidad
de hacerla fuertemente tipada (a nivel de tipos).

En esta charla presentaré algunas de las técnicas de programación
a nivel de tipos. En particular el desarrollo de una estructura
de datos: el registro heterogeneo fuertemente tipado. Éstos consisten en
una colección indizada por tipos (donde la buena formación
se asegura en tiempo de compilación!).

Si es de interés, en una futura sesión puedo presentar una aplicación
de estas técnicas: la (re)implementación de AspectAG, un lenguaje de domino
específico empotrado en Haskell que implementa gramáticas de atributos,
una técnica usada para asociar semántica a gramáticas libres de contexto
(util por ejemplo en la construcción de compiladores).
AspectAG utiliza extensivamente la programación a nivel de tipos
y en particular la estructura de registro heterogeneo para asegurar
ciertas propiedades de buena formación de las gramáticas construidas.
