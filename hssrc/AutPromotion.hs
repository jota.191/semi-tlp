{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE TemplateHaskell      #-}
{-# LANGUAGE KindSignatures       #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}

import Data.Singletons
import Data.Singletons.TH


$(promote [d|
  data Nat
   = Zero
   | Succ Nat
 |])

$(promote [d|
  add :: Nat -> Nat -> Nat
  add Zero n     = n
  add (Succ m) n = Succ (add m n)
 |])
