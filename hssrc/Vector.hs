{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE TemplateHaskell      #-}
{-# LANGUAGE KindSignatures       #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE PolyKinds            #-}

--import GHC.TypeLits (Type)
import Data.Kind

-- | Naturales unarios, usamos también el tipo promovido
data Nat
  = Z
  | S Nat
  deriving (Show, Eq, Ord)


-- | Suma a nivel de tipos
type family m :+ n where
  Z :+ n    = n
  S m :+ n  = S (m :+ n)

-- | GADT de vectores indizados por su largo
data Vec :: Nat -> Type -> Type where
  VZ   :: Vec Z a
  (:<) :: a -> Vec n a -> Vec (S n) a 

infixr 4 :<

v1 = 3 :< 4+3 :< 2 :< VZ


-- | head segura
vHead :: Vec (S n) a -> a
vHead (a :< _)
  = a

-- | Append, no es trivial que esta definición funcione
vAppend :: Vec n a -> Vec m a -> Vec (n :+ m) a
vAppend VZ v
  = v
vAppend (a :< as) v
  = a :< vAppend as v

v2 = v1 `vAppend` v1

-- | Singletons para Nat
data SNat :: Nat -> Type where
  SZ :: SNat Z
  SS :: SNat n -> SNat (S n)


-- | VChop requiere pattern matching sobre un largo, usamos el parámetro
-- singleton explícitamente
vChop :: SNat m -> Vec (m :+ n) a -> (Vec m a, Vec n a) 
vChop SZ as
  = (VZ, as)
vChop (SS sn) (a :< as)
  = let (chp1, chp2)  = vChop sn as
    in  (a :< chp1, chp2)

chp = vChop (SS (SS SZ)) v2

-- | Proxy
data Proxy :: k -> Type where
  Proxy :: Proxy k

vTake :: Proxy n -> SNat m -> Vec (m :+ n) a -> Vec m a
vTake _ SZ _
  = VZ 
vTake proxy (SS n) (a :< as)
  = a :< vTake proxy n as
  
-- | instancia de Show para los vectores
instance Show a => Show (Vec n a) where
  show VZ         = "[]"
  show (a :< VZ)  = "[" ++ show a ++ "]"
  show (a :< as)  = let a_   = show a
                        as_  = show as
                    in   "[" ++ a_ ++ ", " ++ tail as_
