TEX = pdflatex -shell-escape -interaction=nonstopmode -file-line-error

.PHONY: all view

all : pdf view

view : Main.tex
	evince Main.pdf&

pdf : Main.lhs Main.tex
	lhs2TeX Main.lhs > Main.tex
	$(TEX) Main.tex

.PHONY: clean
clean:
	rm -f *.log *.aux *~ *.pdf *bbl
